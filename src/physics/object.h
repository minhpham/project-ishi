#ifndef PHYSICS_OBJECT_H_
#define PHYSICS_OBJECT_H_

#include <core/vector.h>
#include <core/point.h>

namespace ishi {

/// Physically collidable object
class Object {
 public:
  virtual ~Object() {}

  virtual float Mass() const = 0;
  virtual Point Barycenter() const = 0;
  virtual Vector Velocity() const = 0;
  virtual Vector Spin() const = 0;
};

}  // namespace ishi

#endif

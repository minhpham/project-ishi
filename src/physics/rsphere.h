#ifndef PHYSICS_SPHERE_H_
#define PHYSICS_SPHERE_H_

#include <core/point.h>
#include <core/vector.h>
#include <physics/object.h>

namespace ishi {

class CollisionData;

class RSphere : public Object {
 public:
  float r;    // radius
  Point c;    // center
  float m;    // mass
  Vector v;   // velocity
  Vector s;   // spin

 public:
  /// Initialize a physics sphere on the xy plane
  RSphere(float mass, float radius, float cx, float cy);

  /// Initialize a physics sphere in space
  RSphere(float mass, float radius, float cx, float cy, float cz);

  virtual float Mass() const;

  virtual Point Barycenter() const;

  virtual Vector Velocity() const;

  virtual Vector Spin() const;

  CollisionData* Collide(const RSphere &s) const;
};

}  // namespace ishi

#endif

# Test programs
bin_PROGRAMS = core/point_test \
               core/vector_test \
               core/point_vector_test \
               core/matrix_test \
               core/transform_test \
               core/transform_point_test \
               core/transform_vector_test \
               shapes/triangle_test

# Build flags
AM_LDFLAGS = --coverage
AM_CPPFLAGS = -I$(top_srcdir)/lib \
              -I$(top_srcdir)/src \
              -g --coverage -Wall
LDADD = $(top_srcdir)/lib/libUnitTest++.a

# Source files
core_point_test_SOURCES = core/main.cpp \
                          core/point_test.cpp \
                          ../src/core/point.cpp \
                          ../src/core/vector.cpp

core_vector_test_SOURCES = core/main.cpp \
                           core/vector_test.cpp \
                           ../src/core/vector.cpp

core_point_vector_test_SOURCES = core/main.cpp \
                                 core/point_vector_test.cpp \
                                 ../src/core/point.cpp \
                                 ../src/core/vector.cpp

core_matrix_test_SOURCES = core/main.cpp \
                           core/matrix_test.cpp \
                           ../src/core/matrix.cpp

core_transform_vector_test_SOURCES = core/main.cpp \
                                     core/transform_vector_test.cpp \
                                     ../src/core/transform.cpp \
                                     ../src/core/matrix.cpp \
                                     ../src/core/point.cpp \
                                     ../src/core/vector.cpp \
                                     ../src/core/bbox.cpp

core_transform_point_test_SOURCES = core/main.cpp \
                                    core/transform_point_test.cpp \
                                    ../src/core/transform.cpp \
                                    ../src/core/matrix.cpp \
                                    ../src/core/point.cpp \
                                    ../src/core/vector.cpp \
                                    ../src/core/bbox.cpp

core_transform_test_SOURCES = core/main.cpp \
                              core/transform_test.cpp \
                              ../src/core/transform.cpp \
                              ../src/core/matrix.cpp \
                              ../src/core/point.cpp \
                              ../src/core/vector.cpp \
                              ../src/core/bbox.cpp

shapes_triangle_test_SOURCES = shapes/main.cpp \
                               shapes/triangle_test.cpp \
                               ../src/core/transform.cpp \
                               ../src/core/matrix.cpp \
                               ../src/core/point.cpp \
                               ../src/core/vector.cpp \
                               ../src/core/bbox.cpp \
                               ../src/core/shape.cpp \
                               ../src/shapes/triangle.cpp \
                               ../src/shapes/quad.cpp \
                               ../src/shapes/trianglemesh.cpp

# Relevant source directories
coredir = $(top_srcdir)/src/core
shapesdir = $(top_srcdir)/src/shapes

# Build targets
check: all
	@echo 'Executing unittests ...'
	@for prog in $(bin_PROGRAMS); do \
		echo 'Executing '$$prog; \
		./$$prog; \
	done;

coverage: check
	@echo 'Generating coverage info ...'
	lcov --capture --directory $(coredir) --directory $(shapesdir) --output-file coverage.info
	lcov --remove coverage.info "/usr*" -o coverage.info
	lcov --remove coverage.info "*lib/UnitTest++*" -o coverage.info
	genhtml coverage.info --output-directory cov-html
